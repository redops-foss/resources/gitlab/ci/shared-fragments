# Shared Gitlab CI Fragments

> Redops FOSS

A collection of jobs fragments you can use in your own projects.
Made possible by [this feature](https://docs.gitlab.com/ee/ci/yaml/includes.html).

## Usage

### Fragments picking

You need to include the file with the fragment you want to use.
Then you can use it to declare one of your jobs.

```yaml
include:
  - project: "redops-foss/resources/gitlab/ci/shared-fragments"
    ref: "1.0.0" # a tag or a branch name (prefer a tag)
    file: "fragments/node.yml"

stages:
  - test

node:lint:
  stage: test
  extends:
    - .redops:node:lint
```

This will import our fragments file in your pipeline and allow you to define jobs by extending our fragments.

### Pipeline template

```yaml
include:
  - project: "redops-foss/resources/gitlab/ci/shared-fragments"
    ref: "1.0.0" # a tag or a branch name (prefer a tag)
    file: "templates/javascript.yml"
```

This will include all jobs from our template.

You can override variables, jobs, etc... as documented in [this link](https://docs.gitlab.com/ee/ci/yaml/includes.html)

## Versioning

We use [SemVer](http://semver.org/) for versioning.

For the versions available, see the [link to tags on this repository](/tags).
